Ext.define('App.view.todo.ToDoListController',{
	extend: 'Ext.app.ViewController',
	alias: 'controller.todolist',
	views: ['App.view.todo.ToDoList'],

	init: function() {
		var paramStore = Ext.data.StoreManager.lookup('reportmetastore2').getAt(0).getParameters();
		var paramNumber = paramStore.getCount();
		for(var i = 0;  i < paramNumber; i++) {
			this.addNewParamRow(paramStore.getAt(i));
		}
	//	var inFor = this.view;
	//	this.addWindow();
//		var inFor = Ext.getCmp('parametres_id');
//		console.log(inFor);
//		for(var key in inFor) {
//			console.log('name: ' + key + '		value:  ' + inFor[key]);			
//		};
		var me = this;
		Ext.getCmp('returnbutton').on('click', function (event,target){
			me.onReturn(event, target)
		});
		Ext.getCmp('doSomeButton').on('click', function (event, target){
			me.onDoSomeClick(event, target)
		});
	},

	beforeInit: function() {
		this.view.add([{
				xtype: 'window',
		   	title: 'MetaData',
		   	closable: false,
		   	collapsible: true,
				buttonAlign: 'center',
				fbar: [{
					type: 'button',
					minWidth: 80,
					text: 'DoSome',	
	//				handler: 'onDoSomeClick',
					id: 'doSomeButton'
				}],
		  		x: 10, y: 10,
		   	defaults: {
		      	//width: 420,
		      	height: 600,
		      	autoShow: true
		   	},
				align: 'stretch',
				minWidth: 400,
				minHeight: 100,
				flex: 1,
				scrollable: true,
		   	items: [{
					xtype: 'tabpanel',
					flex: 1,
					align: 'fit',
					defaults: {
						bodyPadding: 10,
						scrollable: true	
					},
					items: [{
						title: 'Parameters',
						items: [{
							xtype: 'container',
							layout: 'form',
							id: 'parametres_id',
							align: 'stretch',
							flex: 1
						}]
					}, {
						title: 'Tables',
						items: [{
							xtype: 'gridpanel',
							flex: 1,
							align: 'stretch',
							store: Ext.data.StoreManager.lookup('reportmetastore2').getAt(0).getTables(),
							columns: [{
								text: 'Name',
								dataIndex: 'Name',
								width: 150,
								hideable: false
							}, {
								text: 'Caption',
								dataIndex: 'id',
								width: 215,
								renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
									return record.getCaptions().getAt(0).get('Value');
								}
							}],
							width: 400,
							height: 500,
							scrollable: true,
							leadingBufferZone: 8,
							trailingBufferZone: 8,
							title: "Report's tables",
							plugins: [{
								ptype: 'rowwidget',
								widget: {
									xtype: 'grid',
									autoLoad: true,
									bind: {
										store: '{record.getColumns}',
										title: 'Columns for {record.Name}'
									},
									columns: [{
										text: 'Name',
										dataIndex: 'Name',
										width: 150
									}, {
										text: 'Show',
										width: 175,
										dataIndex: 'Show',
										xtype: 'checkcolumn',
										invert: true
									}]
								}	
							}]	
						}]
					}]
				}]
		}]);
	},

	onDoSomeClick: function() {
		var paramContainer = Ext.getCmp('parametres_id');
		var myQueryElements = Ext.ComponentQuery.query('textfield[fieldLabel="report_id"]');
		var val = myQueryElements[0].getValue();
		console.log(myQueryElements);
		console.log(paramContainer);
		console.log(val);
		console.log('XXXXXXXXX');
		var columnStore = Ext.data.StoreManager.lookup('reportmetastore2').getAt(0).getTables().getAt(0).getColumns();
		for(var i = 0; i < columnStore.getCount(); i++) {
			console.log(columnStore.getAt(i).get('Name') + ': ' + columnStore.getAt(i).get('Show'));
		}
	},		

	addRowSTR: function(record, isMandatoryMark) {
		Ext.getCmp('parametres_id').add([{
			xtype: 'container',
			layout: 'hbox',
			flex: 1,
			items: [{
				xtype: 'textfield',
				name: record.get('Name') + 'Value',
			//	flex: 1,
				allowBlank: false,
				fieldLabel: record.get('Name') + isMandatoryMark,
				labelWidth: 200,
				flex: 1			
			}]		
		}]);
	},	

	addNewParamRow: function(record) {
		var isMandatoryMark; 
		if(record.get('IsMandatory')) {
			isMandatoryMark = '*';
		}
		else {
			isMandatoryMark = '';
		}
		if(record.get('DataType') == 'STR') {
			this.addRowSTR(record, isMandatoryMark);
		} else if(record.get('DataType') == 'INT') {
			this.addRowINT(record, isMandatoryMark);		
		} else if(record.get('DataType') == 'DAT') {
			this.addRowDAT(record, isMandatoryMark);
		} else if(record.get('DataType') == 'DTM') {
			this.addRowDTM(record, isMandatoryMark);
		} else if(record.get('DataType') == 'BOL') {
			this.addRowBOL(record, isMandatoryMark);
		} else {
			this.addUnknownRow(record, isMandatoryMark);
		}
	},

	addRowDAT: function(record, isMandatoryMark) {
		Ext.getCmp('parametres_id').add([{
			xtype: 'container',
			layout: 'hbox',
			items: [{
				xtype: 'datefield',
				name: record.get('Name') + 'Value',
				fieldLabel: record.get('Name') + isMandatoryMark,
				labelWidth: 200,
				flex: 1
			}]
		}]);
	},

	addRowBOL: function(record, isMandatoryMark) {
		Ext.getCmp('parametres_id').add([{
			xtype: 'container',
			layout: 'hbox',
			items: [{
				xtype: 'combobox',
				reference: record.get('Name') + 'Reference',
				fieldLabel: record.get('Name') + isMandatoryMark,
				displayField: 'text',
				labelWidth: 200,
				store: Ext.create('Ext.data.ArrayStore', {
							storeId: 'store' + record.get('Name'),
							fields: ['value', 'text'],
							flex: 1,
							data: [
								['true','Yes'],
								['false','No']
							]
				})			
			}]
		}]);
	},

	addRowDTM: function(record, isMandatoryMark) {
		Ext.getCmp('parametres_id').add([{
			xtype: 'container',
			layout: 'hbox',
			items: [{
				xtype: 'datefield',
				name: record.get('Name') + 'Value1',
				fieldLabel: record.get('Name') + ' date:' + isMandatoryMark,
				labelWidth: 200,
				flex: 1
			}]
		}]);
		this.view.add([{
			xtype: 'container',
			layout: 'hbox',
			items: [{
				xtype: 'timefield',
				name: record.get('Name') + 'Value2',
				fieldLabel: ' time' + isMandatoryMark,
				labelWidth: 200,
				flex: 1
			}]
		}]);
	},

	addRowINT: function(record, isMandatoryMark) {
		Ext.getCmp('parametres_id').add([{
			xtype: 'container',
			layout: 'hbox',
			items: [{
				xtype: 'numberfield',
				name: record.get('Name') + 'Value',
				fieldLabel: record.get('Name') + isMandatoryMark,
				allowBlank: false,
				labelWidth: 200,
				flex: 1
			//	width: 95,
			//	value: 5,
			//	maxValue: 10
			}]
		}]);
	},

	addUnknownRow: function(record, isMandatoryMark) {
		Ext.getCmp('parametres_id').add([{
			xtype: 'container',
			layout: 'hbox',
			items: [{
				html: 'UnknownParameter! Name:' + record.get('Name')  + isMandatoryMark
			}]
		}]);
	},

	onReturn: function() {
		this.getView().destroy();
		Ext.create({
			xtype: 'app-main'
		});
	}
});
