Ext.define('App.view.todo.ToDoList',{
	extend: 'Ext.container.Container',
	requires: [
		'App.view.todo.ToDoListController',
		'App.view.todo.ToDoListModel',
		'Ext.plugin.Viewport'
	],
	xtype: 'xtodolist',
	controller: 'todolist',
	plugins: 'viewport',
	viewModel: 'todolist',
   defaults: {
        autoShow: true
    },

   items: [{
				xtype: 'button',
				name: 'return',
				text: 'Return',
				id: 'returnbutton',
				handler: 'onReturn'		
			}]
});
