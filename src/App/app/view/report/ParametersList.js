Ext.define('App.view.report.ParametersList', {
	extend: 'Ext.grid.Panel',
	xtype: 'paramlist',
	
	requires: [
		'App.model.parameter.Parameter',
		'App.model.parameter.MasterLink',
		'App.model.Report',
		'Ext.grid.plugin.CellEditing'
	],	
	
//////////////////////////////////////////////////////////////////////////
	initComponent: function() {
		this.editing = Ext.create('Ext.grid.plugin.CellEditing');
		Ext.apply(this, {
			iconCls: 'icon-grid',
			frame: true,
			plugins: [this.editing],
			autoLoad: true,
			scrollable: true,
			height: 600,
			title: 'Parameters',
			columns: [
				{ text: 'Name', flex: '1', dataIndex: 'Name' },
				{ text: 'DataType', dataIndex: 'DataType'},	
				{
					text: 'Value',
					dataIndex: 'Value',
					field: {
						type: 'textfield'
					},
					renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
						if(record.get('DataType')==='STR'){
							return 'here must be a string';
						} else {
							return '-';
						}
					}	
				}	
			],
			tools: [{
				type: 'plus'	
			}],

			listeners: {
				render: function(cmp) {
					cmp.getView().bindStore(Ext.getStore('reportmetastore2').getAt(0).getParameters());
					console.log('listened!!!!!!!!');			
				}
			}
		});		
		this.callParent();
			
	}
//////////////////////////////////////////////////////////////////////////

	
	
});
