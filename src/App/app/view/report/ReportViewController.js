Ext.define('App.view.report.ReportViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.report',

	init: function() {
		
	},	

	onReturnClick: function() {
		this.getView().destroy();

		Ext.create({
			xtype: 'app-main'
		});
	},

	renderView: function() {
		this.getView().render();
	}		
});
