Ext.define('App.view.report.ReportView', {
	extend: 'Ext.tab.Panel',
	xtype: 'reportView',
	
	requires: [
		'App.view.report.ReportViewController',
		'Ext.plugin.Viewport',
		'Ext.window.MessageBox',
		'App.view.report.ParametersList',
		'App.view.report.ReportViewModel'
	],
	controller: 'report',
	viewModel: 'report',
	plugins: 'viewport',
		
	ui: 'navigation',
	
    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list',
		  items: [{
				xtype: 'button',
				text: 'Return',
				margin: '10 0',
				handler: 'onReturnClick'
		  }]
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Reports',
        iconCls: 'fa-cog',
        items: [{
           // xtype: 'mainlist'
				xtype: 'paramlist'
        }]
    }]
});
