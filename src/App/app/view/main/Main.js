/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('App.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.grid.plugin.RowWidget',
        'Ext.window.MessageBox',

			'App.view.main.MainController',
			'App.view.main.MainModel',
			'App.view.main.List',
			'App.view.main.ReportsList',
			'App.view.main.GapSetList',
			'App.view.todo.ToDoList'
			//'App.view.main.ParametersList'
    ],

    controller: 'main',
    viewModel: 'main',
	 plugins: 'viewport',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list',
		  items: [{
				xtype: 'button',
				text: 'Logout',
				margin: '10 0',
				handler: 'onClickButton'
		  },{
				xtype: 'button',
				text: 'GetDateGapSets',
				margin: '10 0',
				handler: 'onClickButton4'
		  },{
				xtype: 'button',
				text: 'ToDo',
				margin: '10 0',
				handler: 'onToDo'
		  },{
                xtype: 'button',
           	    text: 'TestStore',
           	    margin: '10 0',
           	    handler: 'onLoadTestStore'
          }]
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,
        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Reports',
        iconCls: 'fa-home',
        // The following grid shares a store with the classic version's grid as well!
        items: [{
           // xtype: 'mainlist'
				xtype: 'reportslist'
        }]
    }, {
        title: 'TwoInOne',
        iconCls: 'fa-user',
        items: [{
            //xtype: 'mainlist'
				xtype: 'reportslist'
        }, {
				xtype: 'gapsetlist'
		  }]
    }, {
        title: 'DateGaps',
        iconCls: 'fa-cog',
        items: [{
            //xtype: 'mainlist'
				xtype: 'gapsetlist'
        }]
    }, {
        title: 'Settings',
        iconCls: 'fa-cog',
        items: [{
            xtype: 'mainlist'
				//xtype: 'paramlist'
        }]
    }]
});
