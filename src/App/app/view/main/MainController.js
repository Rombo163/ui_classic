/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('App.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

	requires: [
		'App.model.*',
		'App.store.*',
		'Ext.data.soap.Proxy',
		'Ext.data.soap.Reader'
	],

	//store: 'reportsStore',

    onItemSelected: function (sender, record) {		

	//	Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
		
		var reportName = record.get('Name');
		console.log(reportName);
      this.showReportMeta(reportName);
    },

    onConfirm: function (choice) {
        	if (choice === 'yes') {
				//Do something
        	}
    },

	showReportMeta: function (reportName) {
		var me = this;
		var metaStore = Ext.getStore('metaStore');
		metaStore.load({
			params: {
			//	ReportName: 'PL_TradeCashFlowTill',
				ReportName: reportName,
				IncludeSlaves: false
			},
			callback: function(req,r) {
				var xmlString = '<rootTag>' + metaStore.getAt(0).get('MetadataString') + '</rootTag>';
				var xmlDocument;
				if (window.DOMParser) {
					 xmlDocument = (new DOMParser()).parseFromString(xmlString, 'text/xml');
				} else {
					 xmlDocument = new ActiveXObject("Microsoft.XMLDOM");
					 xmlDocument.async = false;
					 xmlDocument.loadXML(xmlString);
				}

				var reportMetaStore = Ext.create('Ext.data.Store', {
					data: Ext.data.StoreManager.lookup('metaStore').getAt(0).get('xmlDocument'),
					autoLoad: true,
					autoSync: true,
					model: 'App.model.Report',
					storeId: 'reportmetastore2',
					proxy: {
						type: 'memory',
						reader: {
							type: 'xml',
							record: 'rs|Report',
							namespace: ''			
						}
					},
					listeners: {
						load: function (store, records, options) {
							//Do something		
						}
					}
				}); 	
				reportMetaStore.load({
					callback: function(req, r) {
						console.log('############inside: beforeInit -> reportMetaStore.load#############');
						console.log(Ext.data.StoreManager.lookup('reportmetastore2'));
						console.log(Ext.data.StoreManager.lookup('metaStore').getAt(0).get('xmlDocument'));
						console.log('###################################################################');
						me.getView().destroy();
						Ext.create({
							xtype: 'reportView'
						});
					}	
				});				
			}
		});
	},

    onLoadTestStore: function() {
        var me = this;
        var meta = Ext.data.StoreManager.lookup('reportmetastore2').getAt(0);
        var parameters = [];
        for(var i = 0; i < meta.getParameters().getCount(); i++) {
            currentParam = meta.getParameters().getAt(i);
            if(currentParam.get('IsMandatory')) {
                switch (currentParam.get('DataType')) {
                    case 'STR':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': 'someString'
                        });
                        break;
                    case 'DAT':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': '2016-02-02'
                        });
                        break;
                    case 'DTM':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': '2016-01-01T10:00:00'
                        });
                        break;
                    case 'BOL':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': false
                        });
                        break;
                    case 'INT':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': 1
                        });
                        break;
                    case 'SHR':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': 1
                        });
                        break;
                    case 'LNG':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': 1
                        });
                        break;
                    case 'DEC':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': 1
                        });
                        break;
                    case 'DBL':
                        parameters.push({
                            'Name': currentParam.get('Name'),
                            'Value': 1
                        });
                        break;
                    default:
                        console.log('Unknown parameter type!');
                        break;
                }
            }
        }

        var tableDataRequests = [];
        tableDataRequests.push({
            'Table':'Main',
            'Type':'ALL'
        });

        var store = Ext.getStore('testStore');
        store.load({
            params: {
                TableDataRequests: tableDataRequests,
                requestId: me.uniqueID(),
                Name: meta.get('Name'),
                Parameters: parameters
            },
            callback: function(req, r) {
                console.log('**************');
                console.log(store.getCount());
                console.log('**************');
            }
        });
    },

    uniqueID: function(){
        function chr4(){
            return Math.random().toString(16).slice(-4);
        }
        return chr4() + chr4() +
            '-' + chr4() +
            '-' + chr4() +
            '-' + chr4() +
            '-' + chr4() + chr4() + chr4();
    },


	onToDo: function() {
		this.getView().destroy();
		Ext.create({
			xtype: 'xtodolist'
			//xtype: 'xwindows'
		});
	},

	onClickButton: function () {
        // Remove the localStorage key/value
        localStorage.removeItem('TutorialLoggedIn');

        // Remove Main View
        this.getView().destroy();

        // Add the Login Window
        Ext.create({
            xtype: 'login'
        });
    },
	
	onClickButton4: function () {
		var gapSetStore = Ext.getStore('dateGapSetStore');
		gapSetStore.load({
			callback: function (req, r) {
				console.log('>>>>>>>>>>>GapSetsLoaded:>>>>>>>>>>>>>>');
				console.log(gapSetStore.getCount());
				console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
			}
		});
	},	

	beforeInit: function () {
//	onClickButton2: function() {
	var store = Ext.getStore('reportsStore');	
	store.load({
		 callback: function(req,r) {		  
			console.log('>>>>>>>>>>>>>Inside beforeInit func>>>>>>>>>>>>>>>>');	
		  	console.log(store.getCount()); 					
			  for(var key in store.getAt(0).data) {
					console.log('name:   ' + key + '						value:  ' + store.getAt(0).data[key]);			
		   };	 
			console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
		} 
	});			
   }
});
