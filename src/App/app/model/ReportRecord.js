Ext.define('App.model.ReportRecord', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader', 'App.model.Value'],
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Name', mapping: 'Name', type: 'string'}
	],
	hasMany: [
	 	{
			model: 'App.model.Value',
			name: 'getCaptions',
			associationKey: 'Caption'
		}, {
			model: 'App.model.Value',
			name: 'getComments',
			associationKey: 'Comment'
		}
	]
});
