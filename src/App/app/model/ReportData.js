Ext.define('App.model.ReportData', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader'],
	extend: 'Ext.data.Model',
	xtype: 'reportdata',
	autoload: true,
	fields: [
		{
			name: 'reportId',
			mapping: '@reportId',
			type: 'string'
		},{
			name: 'requestId',
			mapping: '@requestId',
			type: 'string'
		}
	]
});
