Ext.define('App.model.table.Table', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader', 'App.model.Value', 'App.model.table.Column'],
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Name', mapping: 'Name', type: 'string'}
	],
	hasMany: [
		{model: 'App.model.Value', name: 'getCaptions', associationKey: 'Caption'},
		{model: 'App.model.Value', name: 'getComments', associationKey: 'Comment'},
		{model: 'App.model.table.Column', name: 'getColumns', associationKey: 'Columns'},
		{model: 'App.model.table.Filter', name: 'getFilters', associationKey: 'Filters'}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'Table',
			rootProperty: '/'		
		}
	}


});
