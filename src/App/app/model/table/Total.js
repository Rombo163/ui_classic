Ext.define('App.model.table.Total', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader'],
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Function', mapping: 'Function', type: 'string'},
		{name: 'KeyColumn', mapping: 'KeyColumn', type: 'string'},
		{name: 'NullStrategy', mapping: 'NullStrategy', type: 'string'},
		{name: 'NullReplaceValue', mapping: 'NullReplaceValue', type: 'string'},
		{name: 'NullText', mapping: 'NullText', type: 'string'},
		{name: 'Expression', mapping: 'Expression', type: 'string'},
		{name: 'OutputFormat', mapping: 'OutputFormat', type: 'string'},
		{name: 'ShowUntilGroup', mapping: 'ShowUntilGroup', type: 'string'}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'Total',
			rootProperty: 'Totals'
		}
	}
});
