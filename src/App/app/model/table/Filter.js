Ext.define('App.model.table.Filter', {
	requires: ['Ext.data.reader.Xml'],
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Name', mapping: '@name', type: 'string'},
		{name: 'FilterGroups', mapping: '@filterGroups', type: 'boolean'},
		{name: 'AffectTotals', mapping: '@affectTotals', type: 'boolean'},
		{name: 'Value', mapping: '/', type: 'string'}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'Filter',
			rootProperty: '/'
		}
	}
});
