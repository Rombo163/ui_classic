Ext.define('App.model.Report', {
	requires: [
					'Ext.data.reader.Xml',
					'Ext.data.soap.Reader',
					'App.model.Value',
					'App.model.DateGapSet',
					'App.model.table.Table',
					'App.model.parameter.Parameter'
	],	
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Name', mapping: 'Name', type: 'string'},
		{name: 'StoredProcedure', mapping: 'StoredProcedure', type: 'string'},
		{name: 'CleanupStoredPocedure', mapping: 'CleanupStoredProcedure', type: 'string'}
	], 
	hasMany: [
		{model: 'App.model.Value', name: 'getCaptions', associationKey: 'Caption'},
		{model: 'App.model.Value', name: 'getComments', associationKey: 'Comment'},
		{model: 'App.model.parameter.Parameter', name: 'getParameters', associationKey: 'Parameters'},
		{model: 'App.model.table.Table', name: 'getTables', associationKey: 'Tables'},
		{model: 'App.model.DateGapSet', name: 'getDateGapSets', associationKey: 'DateGapSets'}
	]
});
