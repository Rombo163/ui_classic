Ext.define('App.model.DateGap',{
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader'],
	extend: 'Ext.data.Model',
	xtype: 'dategap',
	autoload: true,
	fields: [
		{
			name:	'Name',
			mapping: '@name',
			type: 'string'	
		}, {
			name: 'Period',
			mapping: '@period',
			type: 'string'
		}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'DateGap',
			rootProperty: '/'
		}		
	}
});
