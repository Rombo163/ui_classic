Ext.define('App.model.ReportMetadata', {
	requires: [
		'Ext.data.reader.Xml',
		'Ext.data.soap.Reader'
	],
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'MetadataString',
			mapping: '/',			
			type: 'string'
		}, {
			name: 'xmlDocument',
			mapping: '/',
			type: 'auto',
			convert: function(value, record) {
				var xmlString = '<rootTag>' + value + '</rootTag>';
				var xmlDocument;
				if (window.DOMParser) {
					 xmlDocument = (new DOMParser()).parseFromString(xmlString, 'text/xml');
				} else {
					 xmlDocument = new ActiveXObject("Microsoft.XMLDOM");
					 xmlDocument.async = false;
					 xmlDocument.loadXML(xmlString);
				}
				console.log('----------inside convert func of xmlDocument field-----------');
				//console.log(xmlString);
				console.log(xmlDocument);
				console.log('-------------------------------------------------------------');
				return xmlDocument;
			}
		}
	]
});
