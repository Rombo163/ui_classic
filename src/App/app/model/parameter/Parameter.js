Ext.define('App.model.parameter.Parameter', {
	requires: ['Ext.data.reader.Xml', 'Ext.data.soap.Reader', 'App.model.Value', 'App.model.parameter.MasterLink'],
	extend: 'Ext.data.Model',
	fields: [
		{name: 'Name', mapping: 'Name', type: 'string'},
		{name: 'DataType', mapping: 'DataType', type: 'string'},	
		{name: 'DbName', mapping: 'DbName', type: 'string'},	
		{name: 'IsMandatory', mapping: 'IsMandatory', type: 'boolean'},	
		{name: 'DefaultValue', mapping: 'DefaultValue', type: 'string'},	
		{name: 'IsHidden', mapping: 'IsHidden', type: 'boolean'},
		{name: 'Value'}		
	],
	hasMany: [
		{model: 'App.model.Value', name: 'getCaptions', associationKey: 'Caption'},
		{model: 'App.model.Value', name: 'getComments', associationKey: 'Comment'},
		{model: 'App.model.parameter.MasterLink', name: 'getMasterLinks', associationKey: 'MasterLinks'}
	],
	proxy: {
		type: 'memory',
		reader: {
			type: 'xml',
			record: 'Parameter',
			rootProperty: 'Parameters'
		}
	}
});
