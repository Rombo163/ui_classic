Ext.define('App.store.TemplateTestStore', {
extend: 'Ext.data.Store',
model: 'App.model.ReportData',
storeId: 'testStore',
autoload: true,
	proxy: {
        type: 'soap',
        timeout: 300000,
        url: 'http://alfa133-app.egar.egartech.com:81/reports/services/ReportService',
        api: {
            read: 'GenerateReport'
        },
        soapAction: {
            read: 'http://reports.ws.api.reportservice.egartech.com/ReportWebService/GenerateReportRequest'
        },
		  envelopeTpl: [
        '<?xml version="1.0" encoding="utf-8" ?>',
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:rep="http://reports.ws.api.reportservice.egartech.com/" xmlns:sty="http://reports.ws.api.reportservice.egartech.com/styles/">',//xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"  xmlns:rep="http://reports.ws.api.reportservice.egartech.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">',
            '{[values.bodyTpl.apply(values)]}',
        '</soapenv:Envelope>'
    	 ],
    	 readBodyTpl: [
        '<soapenv:Body>',
            '<rep:{operation}>',
                '<rep:GenerateReportRequest requestId="{params.requestId}">',
                '<Name>{params.Name}</Name>',
               /* '<tpl foreach="params">',
                    '<rep:{$}>{.}</rep:{$}>',
						 // '<{$}:{.}/>',
                '</tpl>',*/
                '<rep:Parameters>',
                    '<tpl for="params.Parameters">',
                        '<rep:Parameter name="{Name}">{Value}</rep:Parameter>',
                    '</tpl>',
                '</rep:Parameters>',
                '<rep:TableDataRequests>',
                    '<tpl for="params.TableDataRequests">',
                        '<rep:TableDataRequest table="{Table}" type="{Type}">',
                        '</rep:TableDataRequest>',
                    '</tpl>',
                '</rep:TableDataRequests>',
                '</rep:GenerateReportRequest>',
            '</rep:{operation}>',
        '</soapenv:Body>'
    	 ],
        //operationParam: 'parameters',
        //targetNamespace: 'http://reports.ws.api.reportservice.egartech.com/',
        reader: {
            type: 'soap',
       		keepRawData: true,
  		      record: 'ReportData',
            namespace: ''
        }
    }
});
