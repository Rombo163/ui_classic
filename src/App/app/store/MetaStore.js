Ext.define('App.store.MetaStore', {
extend: 'Ext.data.Store',
model: 'App.model.ReportMetadata',
storeId: 'metaStore',
autoload: true,
	proxy: {
        type: 'soap',
        url: 'http://alfa133-app.egar.egartech.com:81/reports/services/ReportService',
        api: {
            read: 'GetReportMetadata'
        },
        soapAction: {
            read: 'http://reports.ws.api.reportservice.egartech.com/ReportWebService/GetReportMetadataRequest'
        },
		  envelopeTpl: [
        '<?xml version="1.0" encoding="utf-8" ?>',
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"  xmlns:rep="http://reports.ws.api.reportservice.egartech.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">',
            '{[values.bodyTpl.apply(values)]}',
        '</soapenv:Envelope>'
    	 ],
    	 readBodyTpl: [
        '<soapenv:Body>',
            '<rep:{operation} xmlns="{targetNamespace}">',
                '<tpl foreach="params">',
                    '<rep:{$}>{.}</rep:{$}>',
						 // '<{$}:{.}/>',
                '</tpl>',
            '</rep:{operation}>',
        '</soapenv:Body>'
    	 ],
        //operationParam: 'parameters',
        //targetNamespace: 'http://reports.ws.api.reportservice.egartech.com/',
        reader: {
            type: 'soap',
       		keepRawData: true,
  		      record: 'ns2|ReportMetadata',
            namespace: ''
        }
    }
});
