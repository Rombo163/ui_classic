Ext.define('App.store.DateGapSets', {
extend: 'Ext.data.Store',
model: 'App.model.DateGapSet',
storeId: 'dateGapSetStore',
autoload: true,
	proxy: {
        type: 'soap',
        url: 'http://alfa133-app.egar.egartech.com:81/reports/services/ReportService',
        api: {
            read: 'GetDateGapSets'
        },
        soapAction: {
            read: 'http://reports.ws.api.reportservice.egartech.com/ReportWebService/GetDateGapSetsRequest'
        },
		  envelopeTpl: [
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"  xmlns:rep="http://reports.ws.api.reportservice.egartech.com/" >',
            '{[values.bodyTpl.apply(values)]}',
        '</soapenv:Envelope>'
    	 ],
    	 readBodyTpl: [
		'<soapenv:Header/>',
        '<soapenv:Body>',
            '<rep:{operation}/>',
                '<tpl foreach="params">',
                    '<{$}>{.}</{$}>',
                '</tpl>',
        '</soapenv:Body>'
    	 ],
		 //targetNamespace: 'http://reports.ws.api.reportservice.egartech.com/',
        reader: {
            type: 'soap',
       		keepRawData: true,
  		      record: 'ns2|DateGapSet',
            namespace: ''
        }
    }
});
