Ext.define('App.store.ReportRecords', {
extend: 'Ext.data.Store',
model: 'App.model.ReportRecord',
storeId: 'reportsStore',
autoload: true,
	proxy: {
        type: 'soap',
        url: 'http://alfa133-app.egar.egartech.com:81/reports/services/ReportService',
        api: {
            read: 'GetReportsList'
        },
        soapAction: {
            read: 'http://reports.ws.api.reportservice.egartech.com/ReportWebService/GetReportsListRequest'
        },
		  envelopeTpl: [
        '<?xml version="1.0" encoding="utf-8" ?>',
        		'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"  xmlns:rep="http://reports.ws.api.reportservice.egartech.com/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">',
            '{[values.bodyTpl.apply(values)]}',
        '</soapenv:Envelope>'
    	 ],
    	 readBodyTpl: [
        '<soapenv:Body>',
            '<rep:{operation}/>',
                '<tpl foreach="params">',
                    '<{$}>{.}</{$}>',
						 // '<{$}:{.}/>',
                '</tpl>',
           // '</{operation}>',
        '</soapenv:Body>'
    	 ],
        //operationParam: 'parameteres',
        //targetNamespace: 'http://reports.ws.api.reportservice.egartech.com/',
        reader: {
            type: 'soap',
       		keepRawData: true,
  		      record: 'ns2|ReportSummary',
            namespace: ''
        }
    }
});
